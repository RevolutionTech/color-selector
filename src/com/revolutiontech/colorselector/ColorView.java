/*
 * Name: Lucas Connors
 * Project: Color Selector
 * File: ColorView.java
 */

package com.revolutiontech.colorselector;

import java.util.Observable;
import java.util.Observer;

import android.content.Context;
import android.content.Intent;
import android.util.*;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Button;

public class ColorView extends RelativeLayout implements Observer {
	// widgets
	private Button btnDetails;
	
	// constructor
	public ColorView(Context context, int red, int green, int blue){
		super(context);
		
		Log.d("Color Selector", "ColorView constructor");
		
		/*
		// inflate the view into the display
		View.inflate(context, resColorView, this);
		
		// add this to model's observers
		MyApplication.model.addObserver(this);
		*/
		
		// get widgets
		btnDetails = (Button)findViewById(R.id.btnRedDetails);
		
		// create a controller for the button
		/*
		final Context thisContext = context;
		final int thisRed = red;
		final int thisGreen = green;
		final int thisBlue = blue;  
		btnDetails.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				// tell the model what the color is
				MyApplication.model.setColor(thisRed, thisGreen, thisBlue);
				
				// create intent to request the score activity to be shown
				Intent intent = new Intent(thisContext, DetailActivity.class);
				// start the activity
	            thisContext.startActivity(intent); 
			}
		});
		*/
	}
	
	// update interface
	public void update(Observable observable, Object data){
		Log.d("Color Selector", "update ColorView");
	}
}