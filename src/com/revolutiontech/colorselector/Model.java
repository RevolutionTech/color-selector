/*
 * Name: Lucas Connors
 * Project: Color Selector
 * File: Model.java
 */

package com.revolutiontech.colorselector;

import android.util.Log;
import java.util.Observable;
import java.util.Observer;

public class Model extends Observable {
	private int red;
	private int green;
	private int blue;
	
	// constructor
	public Model(){
		Log.d("Color Selector", "Model constructor");
	}
	
	// getters
	public int getRed(){
		return red;
	}
	public int getGreen(){
		return green;
	}
	public int getBlue(){
		return blue;
	}
	
	// setters
	public void setRed(int red){
		this.red = red;  
	}
	public void setGreen(int green){
		this.green = green;  
	}
	public void setBlue(int blue){
		this.blue = blue;  
	}
	public void setColor(int red, int green, int blue){
		this.red = red;
		this.green = green;
		this.blue = blue;
	}
	
	// Observer methods
	@Override
	public void addObserver(Observer observer) {
		Log.d("Color Selector", "Model: Observer added");
		super.addObserver(observer);
	}

	@Override
	public synchronized void deleteObservers() {
		super.deleteObservers();
	}

	@Override
	public void notifyObservers() {
		Log.d("Color Selector", "Model: Observers notified");
		super.notifyObservers();
	}

	@Override
	protected void setChanged() {
		super.setChanged();
	}

	@Override
	protected void clearChanged() {
		super.clearChanged();
	}
}