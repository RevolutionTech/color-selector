/*
 * Name: Lucas Connors
 * Project: Color Selector
 * File: MainActivity.java
 */

package com.revolutiontech.colorselector;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
	
	@Override
    protected void onPostCreate(Bundle savedInstanceState){
		super.onPostCreate(savedInstanceState);
		
		Log.d("Color Selector", "Main onPostCreate");
		
		Button btnRedDetails = (Button)findViewById(R.id.btnRedDetails);
		
		// create a controller for the button
		final Context thisContext = this;
		btnRedDetails.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				// tell the model which player it is
				// MyApplication.model.setCurrentPlayer(thisPlayer);
				
				// create intent to request the score activity to be shown
				Intent intent = new Intent(thisContext, DetailActivity.class);
				// start the activity
	            thisContext.startActivity(intent); 
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
