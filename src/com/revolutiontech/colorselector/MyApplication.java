/*
 * Name: Lucas Connors
 * Project: Color Selector
 * File: MyApplication.java
 */

package com.revolutiontech.colorselector;

import android.app.Application;

public class MyApplication extends Application {
	// an application-wide reference to the model
    public static Model model = new Model();
}